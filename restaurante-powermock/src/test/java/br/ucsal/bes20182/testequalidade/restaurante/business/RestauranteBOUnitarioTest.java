package br.ucsal.bes20182.testequalidade.restaurante.business;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.ucsal.bes20182.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.bes20182.testequalidade.restaurante.persistence.MesaDao;

@RunWith(PowerMockRunner.class)
@PrepareForTest({MesaDao.class, ComandaDao.class})
public class RestauranteBOUnitarioTest {

	/**
	 * M�todo a ser testado: public static Integer abrirComanda(Integer
	 * numeroMesa) throws RegistroNaoEncontrado, MesaOcupadaException. Verificar
	 * se a abertura de uma comanda para uma mesa livre apresenta sucesso.
	 * Lembre-se de verificar a chamada ao ComandaDao.incluir(comanda).
	 */
	
	@Test
	public void abrirComandaMesaLivre() {
		try {
			Mesa mesa = new Mesa(1);
			PowerMockito.mockStatic(MesaDao.class);
			PowerMockito.when(MesaDao.obterPorNumero(1)).thenReturn(mesa);
			
			PowerMockito.mockStatic(ComandaDao.class);
			
			RestauranteBO.abrirComanda(1);
			
			PowerMockito.verifyStatic();
		} catch (Exception e) {
			assertTrue(false);
		}

	}
}
