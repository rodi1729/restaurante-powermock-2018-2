package br.ucsal.bes20182.testequalidade.restaurante.domain;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Comanda.class)
public class ComandaUnitarioTest {

	/**
	 * M�todo a ser testado: private Double calcularTotal(). Verificar o c�lculo do
	 * valor total, com um total de 4 itens.
	 */

	private Item item;
	private ArrayList<Item> itens;

	@Before
	public void setup() {
		itens = new ArrayList<>();
		item = new Item("caju", 1.0);
		itens.add(item);
		item = new Item("abacaxi", 2.0);
		itens.add(item);
		item = new Item("manga", 3.0);
		itens.add(item);
		item = new Item("caj�", 4.0);
		itens.add(item);

	}

	@Test
	public void calcularTotal4Itens() throws Exception {
		double esperado = 10.0;
		double resultado = 0;
		for (int a = 0; a < 3; a++) {
			item = itens.get(a);
			resultado += item.valorUnitario;
		}

		Comanda mockComanda = PowerMockito.spy(new Comanda(new Mesa(1)));
		PowerMockito.when(mockComanda, "calcularTotal").thenReturn(new Double(10));
		resultado = mockComanda.fechar();
		PowerMockito.verifyPrivate(mockComanda).invoke("calcularTotal");

		Assertions.assertEquals(esperado, resultado);

	}

}
